#define _DEFAULT_SOURCE

#include <unistd.h>


#include "mem.h"
#include "mem_internals.h"
#include "util.h"


#define HEAP_SIZE 10000

static void *test_heap_init() {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("Failed to initialize the heap");
    }
    return heap;
}

void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

void test1(struct block_header *base_block) { //Обычное успешное выделение памяти
    printf("Test 1 has started\n");
    const size_t test_size = 524;
    void *memory = _malloc(test_size);
    if (memory == NULL)
        printf("Test 1 has failed: malloc returns NULL\n");
    debug_heap(stdout, base_block);
    if (base_block->capacity.bytes != test_size || base_block->is_free != false)
        printf("Test 1 has failed \n");
    _free(memory);
    printf("Test 1 has passed\n");
}

void test2(struct block_header *base_block) { //Освобождение одного блока из нескольких выделенных.
    printf("Test 2 has started\n");

    void *m1 = _malloc(5545), *m2 = _malloc(600);
    if (!m1 || !m2)
        printf("Test 2 has failed: malloc returns NULL\n");

    _free(m1);
    debug_heap(stdout, base_block);
    struct block_header *header1 = block_get_header(m1);
    struct block_header *header2 = block_get_header(m2);
    if (!header1->is_free || header2->is_free)
        printf("Test has 2 failed\n");
    _free(m1);
    _free(m2);
    printf("Test 2 has passed\n");
}

void test3(struct block_header *base_block) {//Освобождение двух блоков из нескольких выделенных.
    printf("Test 3 has started\n");
    void *m1 = _malloc(HEAP_SIZE / 10);
    void *m2 = _malloc(HEAP_SIZE / 10);
    void *m3 = _malloc(HEAP_SIZE / 10);
    if (!m1 || !m2 || !m3)
        printf("Test 3 has failed: malloc returns NULL\n");
    _free(m2);
    _free(m1);
    debug_heap(stdout, base_block);
    struct block_header *header1 = block_get_header(m1);
    struct block_header *header2 = block_get_header(m2);
    struct block_header *header3 = block_get_header(m3);

    if (!header1->is_free || !header2->is_free || header3->is_free)
        printf("Test 3 has failed");
    if (header1->capacity.bytes < (HEAP_SIZE / 5) + offsetof(struct block_header, contents))
        printf("Test 3 has failed\n");
    printf("Test 3 has passed\n");
}

void test4(struct block_header *base_block) { //Память закончилась, новый регион памяти расширяет старый.
    printf("Test 4 has started\n");
    void *m1 = _malloc(HEAP_SIZE);
    struct block_header *header1 = block_get_header(m1);
    if (!header1) {
        printf("Test 4 has failed\n");
        return;
    }
    if (header1->capacity.bytes < HEAP_SIZE) {
        printf("Test 4 has failed\n");
        return;
    }
    _free(header1->contents);
    debug_heap(stdout, base_block);
    printf("Test 4 has passed\n");
}

void test5(struct block_header *base_block) { //Память закончилась, старый регион памяти не расширить
    printf("Test 5 has started\n");
    void *m1 = _malloc(HEAP_SIZE * 10);
    if (!m1) {
        printf("Test 5 has failed\n");
        return;
    }
    struct block_header *header = base_block;
    while (header->next != NULL)
        header = header->next;
    debug_heap(stdout, base_block);
    void const *new_addr = (uint8_t *) header + size_from_capacity(header->capacity).bytes;
    map_pages(new_addr, 1000, MAP_FIXED);
    debug_heap(stdout, base_block);
    void *m2 = _malloc(4 * HEAP_SIZE);
    debug_heap(stdout, base_block);
    struct block_header *block = block_get_header(m2);
    if (block == header)
        printf("Test 5 has failed\n");
    _free(m1);
    _free(m2);
    printf("Test 5 has passed\n");
}

bool run_all_tests() {
    struct block_header *base_block = (struct block_header *) test_heap_init();
    test1(base_block);
    test2(base_block);
    test3(base_block);
    test4(base_block);
    test5(base_block);
    return 1;
}

